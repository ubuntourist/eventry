#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  eventry.py
#
#  Copyright 2018 Kevin Cole <ubuntourist@hacdc.org> 2018.10.11
#  ___________________________________________________________________________
#
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation; either version 2 of the License, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program; if not, write to the Free Software Foundation, Inc., 51
#  Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#  ___________________________________________________________________________
#
#  This produces a file containing a list of upcoming (and cancelled) Meetup
#  events in a format suitable for the MediaWiki External Data extension. It
#  is intended to be run as a cron job once or more per day.
#
#  For the finer details on both the request and response values see:
#
#      https://www.meetup.com/meetup_api/docs/:urlname/events/#list
#
#  Important keys in the response (with example values):
#
#      "status":     "upcoming",
#      "time":       1539453600000,
#      "utc_offset": -14400000,
#      "duration":   7200000,
#      "local_date": "2018-10-13",
#      "local_time": "14:00",
#      "name":       "Amateur Radio exam session",
#      "link":       "https://www.meetup.com/hac-dc/events/ltbpdqyxnbrb/",
#
#    duration and utc_offset are in milliseconds (e.g. 2 hrs and -4 hrs)
#    time is in milliseconds since 1970-01-01 00:00:00 a.k.a. the "epoch"
#  ___________________________________________________________________________
#

import os
import sys
import errno                      # Standard Linux C error numbers
import time                       # time zones, date time formats, etc.
import json                       # Encode / decode JSON
import configparser               # Configuration parser
from   os.path import expanduser  # Cross-platform home directory finder
from   urllib  import request     # Retreive via HTTP

__appname__    = "Meetup eventry"
__module__     = ""
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2018"
__copyright__  = "Copyright (C) 2018"
__agency__     = "HacDC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Production"  # "Prototype", "Development" or "Production"


CONFIG  = expanduser("~/.config")
MEETUP  = "https://api.meetup.com/{0}/events?"


def eprint(*args, **kwargs):
    """Simplify printing errors to stderr"""
    print(*args, file=sys.stderr, **kwargs)


def main():
    """La raison d'etre"""

    if os.environ.get("TERM"):
        _ = os.system("clear")
    print("{0} v.{1}\n{2} ({3})\n{4}, {5} <{6}>\n"
          .format(__appname__,
                  __version__,
                  __copyright__,
                  __license__,
                  __author__,
                  __agency__,
                  __email__))

    # Fetch current configuration
    #
    config_files = []
    config_files.append("/etc/eventry.conf")
    config_files.append("{0}/{1}".format(CONFIG, "eventry.conf"))
    config = configparser.ConfigParser()
    config.read(config_files)

    zone      = config["main"]["zone"][1:-1]
    group     = config["main"]["group"][1:-1]
    data      = config["main"]["data"][1:-1]
    wiki      = config["main"]["wiki"][1:-1]
    cancelled = config["main"]["cancelled"][1:-1]
    data      = expanduser(data)
    wiki      = expanduser(wiki)
    items     = config["response"]["items"][1:-1].split(",")

    # Force the time zone for logging
    #
    if os.path.isfile("/usr/share/zoneinfo/{0}".format(zone)):
        os.environ["TZ"] = zone
        time.tzset()
    else:
        eprint("Unknown time zone: {0}. Aborting.\n".format(zone))
        sys.exit(1)

    # Build a Meetup events listing request
    #
    url = MEETUP.format(group)
    for criterion in config["criteria"]:
        try:
            value = config["criteria"].getboolean(criterion)
        except ValueError:
            try:
                value = config["criteria"].getint(criterion)
            except ValueError:
                value = config["criteria"][criterion][1:-1]
        url += "&{0}={1}".format(criterion, value)

    # Prune Meetup response entries for MediaWiki External Data extension
    # bugginess: The extension does not handle nested JSON objects properly,
    # thus producing duplcate keys.
    #
    entries = []
    with request.urlopen(url) as response:
        events = json.loads(response.read().decode("utf-8"))
        for event in events:
            entry = {}           # Prune an entry...
            for item in items:     # ...keeping only useful details
                detail = event[item] if item in event else 0
                entry[item] = detail
            start = event["time"]  + entry["utc_offset"]
            end   = start          + entry["duration"]
            start /= 1000        # milliseconds to seconds
            end   /= 1000        # milliseconds to seconds
            entry["start_date"] = time.strftime("%Y-%m-%d", time.gmtime(start))
            entry["start_time"] = time.strftime("%H:%M",    time.gmtime(start))
            if end == start:
                entry["end_date"] = None   # No end date given
                entry["end_time"] = None   # No end time given
            else:
                entry["end_date"]   = time.strftime("%Y-%m-%d", time.gmtime(end))
                entry["end_time"]   = time.strftime("%H:%M",    time.gmtime(end))

            # Add special CSS styling for cancelled events
            #
            if entry["status"] == "cancelled":
                for item in ("name",
                             "start_date", "start_time",
                             "end_date",   "end_time"):
                    if entry[item]:
                        entry[item] = cancelled.format(entry[item])

            entries.append(entry)

    # Write a calendar for the MediaWiki External Data extension to access
    #
    try:
        with open("{0}/eventry.json".format(data), "w") as calendar:
            calendar.write(json.dumps(entries, indent=2))
    except FileNotFoundError:
        # Later: Use errno.ENOENT or EX_CANTCREAT from sysexits.h?
        eprint("Destination directory \"{0}\" does not exist. Aborting.\n"
               .format(data))
        sys.exit(1)

    # Log the time-stamp for the current run
    #
    updated = {"updated": time.strftime("%Y-%m-%d %H:%M",time.localtime())}
    try:
        with open("{0}/updated.json".format(data), "w") as log:
            log.write("{0}\n".format(json.dumps(updated, indent=2)))
    except FileNotFoundError:
        # Later: Use errno.ENOENT or EX_CANTCREAT from sysexits.h?
        eprint("Destination directory \"{0}\" does not exist. Aborting.\n"
               .format(data))
        sys.exit(1)

    # Force MediaWiki to update the cache
    # (Essentially "$ touch LocalSettings.php")
    #
    try:
        os.utime("{0}/LocalSettings.php".format(wiki))
    except FileNotFoundError:
        pass                      # ...unless we're not really a MediaWiki

    print("Done!  List of events updated successfully!  Yay!\n")
    sys.exit(0)


if __name__ == "__main__":
    main()
